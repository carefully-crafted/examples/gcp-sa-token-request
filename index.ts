import { GoogleAuth, IdTokenClient } from "google-auth-library";
import * as fs from "fs";

const targetAudience =
  "https://europe-west3-bitemap-dev.cloudfunctions.net/upload-url-exchange";

const getClient = async (
  serviceAccountJson: string,
  targetAudience: string,
): Promise<IdTokenClient> => {
  const auth = new GoogleAuth({
    credentials: JSON.parse(serviceAccountJson),
  });
  const client = await auth.getIdTokenClient(targetAudience);
  return client;
};

interface RequestBody {
  files: {
    name: string;
    type: "mandibular" | "maxillary";
  }[];
}

interface ResponseBody {
  files: {
    name: string;
    type: "mandibular" | "maxillary";
    uploadUrl: string;
  }[];
  resultUrl: string;
}

async function main() {
  const serviceAccountJson = fs.readFileSync(
    "./secrets/bitemap-dev-89e591bbb287.json",
    "utf8",
  );

  const payload = <RequestBody>{
    files: [
      {
        name: "MandibularR.stl",
        type: "mandibular",
      },
      {
        name: "MaxillaryR.stl",
        type: "maxillary",
      },
    ],
  };

  const client = await getClient(serviceAccountJson, targetAudience);
  const response = await client.request({
    url: targetAudience,
    method: "POST",
    data: payload,
  });

  const data = response.data as ResponseBody;

  const uploadPromises = data.files.map(async (file) => {
    console.log("Uploading file: ", file.name);
    return await fetch(file.uploadUrl, {
      method: "PUT",
      headers: {
        "Content-Type": "model/stl",
        // allow to be stored in a private cache (e.g. local caches in browsers)
        "Cache-Control": "private",
      },
      body: fs.readFileSync(`./cube.stl`),
    });
  });

  await Promise.all(uploadPromises);
  console.log("Result url: ", data.resultUrl);
}

main();
